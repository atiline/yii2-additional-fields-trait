<?php

namespace atiline\AdditionalFieldsTrait\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "additional_field_table".
 *
 * @property string $element_class_name
 * @property int $element_id
 * @property string $field
 * @property int|null $value_binary
 * @property int|null $value_int
 * @property float|null $value_double
 * @property string|null $value_varchar
 * @property string|null $value_text
 * @property string|null $value_date
 * @property string|null $value_datetime
 * @property int $created_at
 * @property int $updated_at
 */
class AdditionalFieldModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'additional_field_table';
    }

    public static $dbConn = 'db';

    /**
     * @return mixed|\yii\db\Connection
     */
    public static function getDb() {
        $component = static::$dbConn;
        return Yii::$app->$component;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['element_class_name', 'element_id', 'field'], 'required'],
            ['element_id', 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
            [['value_binary', 'value_int'], 'integer'],
            [['value_double'], 'number'],
            [['value_text'], 'string'],
            [['value_date', 'value_datetime'], 'safe'],
            [['element_class_name', 'field'], 'string', 'max' => 100],
            [['element_id'], 'integer'],
            [['value_varchar'], 'string', 'max' => 1024],
            [['element_class_name', 'element_id', 'field'], 'unique', 'targetAttribute' => ['element_class_name', 'element_id', 'field']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'element_class_name' => 'Element Table Name',
            'element_id' => 'Element ID',
            'field' => 'Field',
            'value_binary' => 'Value Binary',
            'value_int' => 'Value Int',
            'value_double' => 'Value Double',
            'value_varchar' => 'Value Varchar',
            'value_text' => 'Value Text',
            'value_date' => 'Value Date',
            'value_datetime' => 'Value Datetime',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class
        ];
    }
}
