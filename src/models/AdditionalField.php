<?php

namespace atiline\AdditionalFieldsTrait\models;

use yii\base\Component;
use yii\base\InvalidArgumentException;


/**
 * Class FrEventCalendarTypeData
 * @package common\models\franchise
 *
 *
 * @property string $type
 * @property string $label
 * @property string $name
 *
 * @property bool $is_required
 *
 * @property bool $is_hidden

 * @property bool $is_disabled
 *
 * @property string $joinTableName
 */
class AdditionalField extends Component {



    public $type;
    public $label;
    public $name;

    public $is_hidden = false;
    public $is_disabled = false;

    public $is_required;


    public function init()
    {
        parent::init();

        if(
            !$this->type
            ||
            !$this->name
            ||
            !$this->label
        ) {
            throw new InvalidArgumentException('Some param not setted');
        }
    }


    const TYPE_BOOLEAN = 'boolean';
    const TYPE_INT = 'int';
    const TYPE_DOUBLE = 'double';
    const TYPE_VARCHAR = 'varchar';
    const TYPE_TEXT = 'text';
    const TYPE_STRING_COLOR = 'string_color';
    const TYPE_STRING_EMAIL = 'string_email';
    const TYPE_DATE = 'date';
    const TYPE_DATETIME = 'datetime';


    public function getJoinTableName() {
        return self::makeJoinTableNameByFieldName($this->name);
    }

    public static function makeJoinTableNameByFieldName($name) {
        return 'aft_' . $name;
    }


    public function makeJoinCondition() {


        $cond = '`' . $this->joinTableName . '`.`';

        switch ($this->type) {
            case AdditionalField::TYPE_BOOLEAN:
                $cond .= 'value_binary';
                break;
            case AdditionalField::TYPE_INT:
                $cond .= 'value_int';
                break;
            case AdditionalField::TYPE_DOUBLE:
                $cond .= 'value_double';
                break;
            case AdditionalField::TYPE_VARCHAR:
            case AdditionalField::TYPE_STRING_EMAIL:
            case AdditionalField::TYPE_STRING_COLOR:
            $cond .= 'value_varchar';
                break;
            case AdditionalField::TYPE_TEXT:
                $cond .= 'value_text';
                break;
            case AdditionalField::TYPE_DATE:
                $cond .= 'value_date';
                break;
            case AdditionalField::TYPE_DATETIME:
                $cond .= 'value_datetime';
                break;
            default:
                break;
        }

        $cond .= '`';
        return $cond;
    }
}