<?php

namespace atiline\AdditionalFieldsTrait\models;


use Codeception\PHPUnit\Constraint\Page;
use yii\db\ExpressionInterface;
use yii\helpers\ArrayHelper;

/**
 * Class ActiveQueryModified
 * @package common\components\AdditionalFieldsTrait\models
 */
class ActiveQueryModified extends \yii\db\ActiveQuery {

    public function additionalFieldWhere($condition, $params = [])
    {
        return parent::where($this->modifyCondition($condition), $params);
    }


    public function additionalFieldAndWhere($condition, $params = [])
    {
        return parent::andWhere($this->modifyCondition($condition), $params);
    }


    public function additionalFieldOrWhere($condition, $params = [])
    {
        return parent::orWhere($this->modifyCondition($condition), $params);
    }


    public function additionalFieldOrderBy($columns) {
        $this->orderBy = $this->additionalColumnNormalizeOrderBy($columns);
        return $this;
    }


    /**
     * Normalizes format of ORDER BY data.
     *
     * @param array|string|ExpressionInterface $columns the columns value to normalize. See [[orderBy]] and [[addOrderBy]].
     * @return array
     */
    protected function additionalColumnNormalizeOrderBy($columns)
    {

        $modelClass = $this->modelClass;

        $data = null;

        $data = null;

        if(method_exists($modelClass, 'getAdditionalFieldsData')) {
            try {
                $refMethod = new \ReflectionMethod($modelClass, 'getAdditionalFieldsData');
                if($refMethod->isStatic()) {
                    $data = $modelClass::getAdditionalFieldsData();
                } else {
                    if(method_exists($modelClass, 'getAdditionalFieldsDataAll')) {
                        $refMethod = new \ReflectionMethod($modelClass, 'getAdditionalFieldsDataAll');
                        if($refMethod->isStatic()) {
                            $data = $modelClass::getAdditionalFieldsDataAll();
                        }
                    }
                }
            } catch (\ReflectionException $e) {

            }

        }

        if(!$data instanceof AdditionalFieldsInfoObject) {
            throw new InvalidArgumentException('Method getAdditionalFieldsData must exist and returns AdditionalFieldsInfoObject obj');
        }


        /** @var AdditionalField[] $fields */
        $fields = $data->fields;

        /** @var AdditionalField[] $fields_indexed */
        $fields_indexed = [];

        foreach ($fields as $field) {
            $fields_indexed[$field->name] = $field;
        }


        if ($columns instanceof ExpressionInterface) {
            return [$columns];
        } elseif (is_array($columns)) {
            $new_columns = [];
            foreach ($columns as $key => $value) {
                if(in_array($key, array_keys($fields_indexed))) {
                    $new_columns[$fields_indexed[$key]->makeJoinCondition()] = $value;
                } else {
                    $new_columns[$key] = $value;
                }
            }
            return $new_columns;
        }

        $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
        $result = [];
        foreach ($columns as $column) {
            if (preg_match('/^(.*?)\s+(asc|desc)$/i', $column, $matches)) {
                if(in_array($matches[1], array_keys($fields_indexed))) {
                    $result[$fields_indexed[$matches[1]]->makeJoinCondition()] = strcasecmp($matches[2], 'desc') ? SORT_ASC : SORT_DESC;
                } else {
                    $result[$matches[1]] = strcasecmp($matches[2], 'desc') ? SORT_ASC : SORT_DESC;
                }
            } else {
                if(in_array($column, array_keys($fields_indexed))) {
                    $result[$fields_indexed[$column]->makeJoinCondition()] = SORT_ASC;
                } else {
                    $result[$column] = SORT_ASC;
                }
            }
        }

        return $result;
    }

    private function modifyCondition($condition) {

        if(is_array($condition)) {

            $new_cond = [];


            $modelClass = $this->modelClass;

            $data = null;

            if(method_exists($modelClass, 'getAdditionalFieldsData')) {
                try {
                    $refMethod = new \ReflectionMethod($modelClass, 'getAdditionalFieldsData');
                    if($refMethod->isStatic()) {
                        $data = $modelClass::getAdditionalFieldsData();
                    } else {
                        if(method_exists($modelClass, 'getAdditionalFieldsDataAll')) {
                            $refMethod = new \ReflectionMethod($modelClass, 'getAdditionalFieldsDataAll');
                            if($refMethod->isStatic()) {
                                $data = $modelClass::getAdditionalFieldsDataAll();
                            }
                        }
                    }
                } catch (\ReflectionException $e) {

                }

            }

            if(!$data instanceof AdditionalFieldsInfoObject) {
                throw new InvalidArgumentException('Method getAdditionalFieldsData must exist and returns AdditionalFieldsInfoObject obj');
            }



            /** @var AdditionalField[] $fields */
            $fields = $data->fields;

            /** @var AdditionalField[] $fields_indexed */
            $fields_indexed = [];

            foreach ($fields as $field) {
                $fields_indexed[$field->name] = $field;
            }

            if (isset($condition[0])) { // operator format: operator, operand 1, operand 2, ...

                $operator = array_shift($condition);

                if(in_array($condition[0], array_keys($fields_indexed))) {
                    $condition[0] = $fields_indexed[$condition[0]]->makeJoinCondition();
                }

                array_unshift($condition, $operator);

                $new_cond = $condition;

            } else {
                foreach ($condition as $key => $value) {

                    if(in_array($key, array_keys($fields_indexed))) {
                        $new_cond[$fields_indexed[$key]->makeJoinCondition()] = $value;
                    }

                }
            }
        }


        return $new_cond;
    }

}