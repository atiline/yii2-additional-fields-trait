<?php

namespace atiline\AdditionalFieldsTrait\models;


use yii\base\Component;
use yii\base\InvalidArgumentException;


/**
 * Class AdditionalFieldsInfoObject
 * @package common\components\AdditionalFieldsTrait\models
 *
 * @property string $id_field_name
 * @property string $element_class_name
 * @property AdditionalField[] $fields
 */
class AdditionalFieldsInfoObject extends Component {




    public $id_field_name = null;
    public $element_class_name = null;
    public $fields = [];


    public function init()
    {
        parent::init();

        if (!$this->id_field_name) {
            throw new InvalidArgumentException('id_field_name must be set');
        }
        if (!$this->element_class_name) {
            throw new InvalidArgumentException('element_class_name must be set');
        }
        if (!is_array($this->fields)) {
            throw new InvalidArgumentException('fields must be an array');
        }

        foreach ($this->fields as $field) {

            if (!$field instanceof AdditionalField) {
                throw new InvalidArgumentException('Some field is not instance of AdditionalField');
            }
        }
    }
}