<?php

namespace atiline\AdditionalFieldsTrait;

use atiline\AdditionalFieldsTrait\models\ActiveQueryModified;
use atiline\AdditionalFieldsTrait\models\AdditionalField;
use atiline\AdditionalFieldsTrait\models\AdditionalFieldModel;
use atiline\AdditionalFieldsTrait\models\AdditionalFieldsInfoObject;
use Yii;
use yii\base\InvalidArgumentException;
use yii\db\ActiveRecord;
use yii\db\Expression;


/**
 * Trait AdditionalFieldsTrait
 * @package common\components\AdditionalFieldsTrait
 *
 *
 * @property string $id_field_name
 * @property string $element_class_name
 * @property AdditionalField[] $fields
 *
 * @property string $ownerClass
 */
trait AdditionalFieldsTrait
{


    private $fields = [];
    private $id_field_name = '';
    private $element_class_name = '';
    private $ownerClass = null;


    public function init()
    {

        parent::init();

        $this->loadAdditionalFields();

        $this->on(ActiveRecord::EVENT_INIT,  [$this, 'parentInit']);
        $this->on(ActiveRecord::EVENT_AFTER_INSERT,  [$this, 'parentAfterSave']);
        $this->on(ActiveRecord::EVENT_AFTER_UPDATE,  [$this, 'parentAfterSave']);
        $this->on(ActiveRecord::EVENT_AFTER_REFRESH,  [$this, 'parentAfterSave']);
        $this->on(ActiveRecord::EVENT_AFTER_DELETE,  [$this, 'parentAfterDelete']);
        $this->on(ActiveRecord::EVENT_AFTER_FIND,  [$this, 'parentAfterFind']);

        $this->makeValuesArr();
    }

    private function loadAdditionalFields() {

        $this->ownerClass = self::class;

        $data = null;

        if(method_exists(self::class, 'getAdditionalFieldsData')) {
            try {
                $refMethod = new \ReflectionMethod(self::class, 'getAdditionalFieldsData');
                if($refMethod->isStatic()) {
                    $data = $this->ownerClass::getAdditionalFieldsData();
                } else {
                    $data = $this->getAdditionalFieldsData();
                }
            } catch (\ReflectionException $e) {

            }

        } else {
            throw new InvalidArgumentException('Model class does not contain method getAdditionalFieldsData');
        }

        if(!$data instanceof AdditionalFieldsInfoObject) {
            throw new InvalidArgumentException('Method getAdditionalFieldsData must return AdditionalFieldsInfoObject obj');
        }

        $this->id_field_name = $data->id_field_name;
        $this->element_class_name = $data->element_class_name;
        $this->fields = $data->fields;
    }




    private $_values = [];

    /**
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_values)) {
            return $this->_values[$name];
        }
        return parent::__get($name);
    }


    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_values)) {
            $this->_values[$name] = $value;
        } else {
            parent::__set($name, $value);
        }
    }


    /**
     * @return array
     */
    public function additionalFieldsAttributeLabels()
    {
        $labels = [];
        foreach ($this->fields as $field) {
            $labels[$field->name] = $field->label;
        }
        return $labels;
    }


    /**
     * @return array
     */
    public function additionalFieldsRules()
    {
        $rules = [];
        foreach ($this->fields as $field) {
            switch ($field->type) {
                case $field::TYPE_BOOLEAN:
                    $rules[] = [[$field->name], 'integer'];
                    $rules[] = [[$field->name], 'in', 'range' => [0, 1]];
                    break;
                case $field::TYPE_INT:
                    $rules[] = [[$field->name], 'integer'];
                    break;
                case $field::TYPE_DOUBLE:
                    $rules[] = [[$field->name], 'number'];
                    break;
                case $field::TYPE_VARCHAR:
                case $field::TYPE_STRING_EMAIL:
                case $field::TYPE_STRING_COLOR:
                    $rules[] = [[$field->name], 'string'];
                    break;
                case $field::TYPE_DATE:
                case $field::TYPE_DATETIME:
                    $rules[] = [[$field->name], 'safe'];
                    break;
                case $field::TYPE_TEXT:
                    $rules[] = [[$field->name], 'string'];
                    break;
                default:
                    break;
            }

            if ($field->is_required) {
                $rules[] = [[$field->name], 'required'];
            }
        }

        return $rules;
    }

    public function additionalFieldsNamesList() {
        $names = [];
        foreach ($this->fields as $field) {
            $names[] = $field->name;
        }
        return $names;
    }


    /**
     * @return AdditionalFieldModel[]
     */
    private function getValuesModelsArr()
    {
        $fields_keys = [];
        foreach ($this->fields as $field) {
            $fields_keys[] = $field->name;
        }

        return AdditionalFieldModel::find()
            ->where(['field' => $fields_keys])
            ->andWhere(['element_id' => $this->owner->{$this->id_field_name}])
            ->andWhere(['element_class_name' => $this->element_class_name])
            ->indexBy('field')
            ->all();
    }


    private function makeValuesArr()
    {
        $values = $this->getValuesModelsArr();

        foreach ($this->fields as $field) {

            switch ($field->type) {
                case AdditionalField::TYPE_BOOLEAN:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_binary : null;
                    break;
                case AdditionalField::TYPE_INT:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_int : null;
                    break;
                case AdditionalField::TYPE_DOUBLE:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_double : null;
                    break;
                case AdditionalField::TYPE_VARCHAR:
                case AdditionalField::TYPE_STRING_EMAIL:
                case AdditionalField::TYPE_STRING_COLOR:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_varchar : null;
                    break;
                case AdditionalField::TYPE_TEXT:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_text : null;
                    break;
                case AdditionalField::TYPE_DATE:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_date : null;
                    break;
                case AdditionalField::TYPE_DATETIME:
                    $this->_values[$field->name] = array_key_exists($field->name, $values) ? $values[$field->name]->value_datetime : null;
                    break;
                default:
                    break;
            }
        }
    }


    public function parentInit()
    {
        if(!$this->fields) {
            $this->loadAdditionalFields();
        }
        $this->makeValuesArr();
    }

    public function parentAfterFind()
    {
        if(!$this->fields) {
            $this->loadAdditionalFields();
        }
        $this->makeValuesArr();
    }

    /**
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function parentAfterDelete()
    {
        $values = $this->getValuesModelsArr();

        foreach ($values as $value) {
            $value->delete();
        }
    }

    public function parentAfterSave()
    {

        if ($this->owner->{$this->id_field_name} === null) {
            throw new InvalidArgumentException('Owner identifier field is null. Must be set');
        }

        $values = $this->getValuesModelsArr();

        foreach ($this->fields as $field) {

            if (array_key_exists($field->name, $this->_values)) {
                if ($this->_values[$field->name] !== null) {

                    if (array_key_exists($field->name, $values)) {
                        $value = $values[$field->name];
                    } else {
                        $value = new AdditionalFieldModel();
                        $value->element_class_name = $this->element_class_name;
                        $value->element_id = $this->owner->{$this->id_field_name};
                        $value->field = $field->name;
                    }

                    switch ($field->type) {
                        case $field::TYPE_BOOLEAN:
                            $value->value_binary = $this->_values[$field->name];
                            break;
                        case $field::TYPE_INT:
                            $value->value_int = $this->_values[$field->name];
                            break;
                        case $field::TYPE_DOUBLE:
                            $value->value_double = $this->_values[$field->name];
                            break;
                        case $field::TYPE_VARCHAR:
                        case $field::TYPE_STRING_EMAIL:
                        case $field::TYPE_STRING_COLOR:
                            $value->value_varchar = $this->_values[$field->name];
                            break;
                        case $field::TYPE_DATE:
                            $value->value_date = $this->_values[$field->name];
                            break;
                        case $field::TYPE_DATETIME:
                            $value->value_datetime = $this->_values[$field->name];
                            break;
                        case $field::TYPE_TEXT:
                            $value->value_text = $this->_values[$field->name];
                            break;
                        default:
                            break;
                    }

                    $value->save();
                } else {
                    if (array_key_exists($field->name, $values)) {
                        $value = $values[$field->name];
                        $value->delete();
                    }
                }
            }

        }
    }


    /**
     * @return ActiveQueryModified
     */
    public static function findWithAdditionalFields()
    {

        $data = null;

        if(method_exists(self::class, 'getAdditionalFieldsData')) {
            try {
                $refMethod = new \ReflectionMethod(self::class, 'getAdditionalFieldsData');
                if($refMethod->isStatic()) {
                    $data = self::getAdditionalFieldsData();
                } else {
                    if(method_exists(self::class, 'getAdditionalFieldsDataAll')) {
                        $refMethod = new \ReflectionMethod(self::class, 'getAdditionalFieldsDataAll');
                        if($refMethod->isStatic()) {
                            $data = self::getAdditionalFieldsDataAll();
                        }
                    }
                }
            } catch (\ReflectionException $e) {

            }

        }

        if(!$data instanceof AdditionalFieldsInfoObject) {
            throw new InvalidArgumentException('Method getAdditionalFieldsData must exist and returns AdditionalFieldsInfoObject obj');
        }


        /** @var ActiveQueryModified $query */
        $query = Yii::createObject(ActiveQueryModified::class, [get_called_class()]);

        /** @var ActiveRecord $modelClass */
        $modelClass = $query->modelClass;

        $fields = $data->fields;
        $element_class_name = $data->element_class_name;
        $id_field_name = $data->id_field_name;


        if (!is_array($fields)) {
            throw new InvalidArgumentException('AdditionalFieldTrait with fields not found.');
        }

        foreach ($fields as $field) {
            if (!$field instanceof AdditionalField) {
                throw new InvalidArgumentException('Some field is not instance of AdditionalField');
            }
        }

        /** @var $fields AdditionalField[] */

        foreach ($fields as $field) {

            $query->leftJoin(
                "`" . AdditionalFieldModel::tableName() . '` aft_' . $field->name,
                new Expression("`" . $modelClass::tableName() . "`.`" . $id_field_name . "` = `" . $field->joinTableName . "`.`element_id`" .
                    " AND `" . $field->joinTableName . "`.`element_class_name` = '" . $element_class_name . "'" .
                    " AND `" . $field->joinTableName . "`.`field` = '" . $field->name . "'"
                )
            );

        }

        return $query;
    }

}