<?php

namespace atiline\AdditionalFieldsTrait\migrations;


use yii\db\Migration;

/**
 * Class m190108_195252_smcityrating
 */
class m_atiline_yii2_additional_fields_trait extends Migration
{
    /**
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $this->execute(file_get_contents(__DIR__ . '/m_atiline_yii2_additional_fields_trait_UP.sql'));
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->execute(file_get_contents(__DIR__ . '/m_atiline_yii2_additional_fields_trait_DOWN.sql'));
    }

}
