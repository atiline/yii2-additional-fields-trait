CREATE TABLE `additional_field_table` (
	`element_class_name` VARCHAR(100) NOT NULL,
	`element_id` BIGINT(20) NOT NULL,
	`field` VARCHAR(100) NOT NULL,
	`value_binary` TINYINT(1) NULL DEFAULT NULL,
	`value_int` BIGINT(20) NULL DEFAULT NULL,
	`value_double` DOUBLE(20,6) NULL DEFAULT NULL,
	`value_varchar` VARCHAR(1024) NULL DEFAULT NULL,
	`value_text` TEXT NULL,
	`value_date` DATE NULL DEFAULT NULL,
	`value_datetime` DATETIME NULL DEFAULT NULL,
	`created_at` INT(11) NOT NULL,
	`updated_at` INT(11) NOT NULL,
	PRIMARY KEY (`element_class_name`, `element_id`, `field`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
;
